$(function(){
  $('img').unveil();
  $('.superbox').SuperBox();

  $('#video-modal').modal('show');

  $('#video-modal').on('shown.bs.modal', function (e) {
    $('#btn-action-photo').removeClass('hidden');
    $('#btn-action-video').addClass('hidden');
  })

  $('#video-modal').on('hide.bs.modal', function(e) {
    $('main').removeClass('video-on');
  });

  $('#video-modal').on('hidden.bs.modal', function (e) {
    $('#btn-action-video').removeClass('hidden');
    $('#btn-action-photo').addClass('hidden');
  })

  $('#video-modal').on('show.bs.modal', function(e) {
    $('main').addClass('video-on');
  });

  $('#btn-action-video').on('click', function (e) {
    e.preventDefault();
    $('#video-modal').modal('show');
  })

  $('#btn-action-photo').on('click', function (e) {
    e.preventDefault();
    $('#video-modal').modal('hide');
  })
});
